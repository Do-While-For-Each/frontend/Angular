export const T7E_APP_LANGUAGE = {
  choose_lang: {
    EN: 'Choose language',
    RU: 'Выберите язык',
  },
  speak_multi_lang: {
    EN: 'Do you speak multiple languages?',
    RU: 'Вы говорите на нескольких языках?',
  },
  help_translate: {
    EN: 'Help us translate!',
    RU: 'Помогите нам с переводом!',
  },
  change_lang: {
    EN: 'Change',
    RU: 'Изменить',
  },
};
