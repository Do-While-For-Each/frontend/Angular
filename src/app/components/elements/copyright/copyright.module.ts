import {NgModule} from '@angular/core';

import {AppCopyrightComponent} from './copyright.component';

@NgModule({
  declarations: [
    AppCopyrightComponent,
  ],
  imports: [],
  exports: [
    AppCopyrightComponent,
  ],
})
export class AppCopyrightModule {
}
