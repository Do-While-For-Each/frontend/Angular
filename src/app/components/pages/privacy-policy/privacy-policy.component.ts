import {Component} from '@angular/core';

import {Lang} from '@app/globals';
import {AppService} from '@app/services/app.service';

@Component({
  selector: 'app-page-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss']
})
export class PagePrivacyPolicyComponent {

  lang = Lang;

  constructor(public app: AppService) {
  }
}
