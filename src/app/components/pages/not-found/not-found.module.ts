import {NgModule} from '@angular/core';

import {PageNotFoundComponent} from './not-found.component';

@NgModule({
  declarations: [
    PageNotFoundComponent,
  ],
  imports: [],
  exports: [
    PageNotFoundComponent,
  ],
})
export class PageNotFoundModule {
}
