import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {PageMainHomeComponent} from './main-home.component';

@NgModule({
  declarations: [
    PageMainHomeComponent,
  ],
  imports: [
    CommonModule,

    RouterModule.forChild([
      {path: '', component: PageMainHomeComponent}
    ])

  ],
  exports: [
    RouterModule,
  ],
})
export class PageMainHomeModule {
}
