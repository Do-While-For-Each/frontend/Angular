import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {PageTermsOfUseComponent} from './terms-of-use.component';

@NgModule({
  declarations: [
    PageTermsOfUseComponent,
  ],
  imports: [
    CommonModule,

    RouterModule.forChild([
      {path: '', component: PageTermsOfUseComponent}
    ])

  ],
  exports: [
    RouterModule,
  ],
})
export class PageTermsOfUseModule {
}
