import {Component} from '@angular/core';

import {Lang} from '@app/globals';
import {AppService} from '@app/services/app.service';

@Component({
  selector: 'app-terms-of-use',
  templateUrl: './terms-of-use.component.html',
  styleUrls: ['./terms-of-use.component.scss']
})
export class PageTermsOfUseComponent {

  lang = Lang;

  constructor(public app: AppService) {
  }
}
