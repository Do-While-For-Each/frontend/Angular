import {NgModule} from '@angular/core';

import {PageSettingsComponent} from './settings.component';

@NgModule({
  declarations: [
    PageSettingsComponent,
  ],
  imports: [],
  exports: [
    PageSettingsComponent
  ],
})
export class PageSettingsModule {
}
