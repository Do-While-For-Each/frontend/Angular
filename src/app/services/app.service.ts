import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable} from 'rxjs';

import {AppCommonStorageEntity} from '../classes/AppCommonStorageEntity';
import {Lang} from '../globals';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  defaultLang = Lang.EN;
  subjLang = new BehaviorSubject<string>(this.defaultLang);

  appCommonStorage: AppCommonStorageEntity;

  constructor() {
    this.init();
  }

  init(): void {
    this.appCommonStorage = new AppCommonStorageEntity().fromStorage();
    if (this.appCommonStorage) {
      this.lang = this.appCommonStorage.lang || this.defaultLang;
    } else {
      this.appCommonStorage = AppCommonStorageEntity.of({lang: this.lang, prevUsername: ''});
    }
  }

  get lang(): string {
    return this.subjLang.getValue();
  }

  get lang$(): Observable<string> {
    return this.subjLang.asObservable();
  }

  set lang(value: string) {
    value = Object.keys(Lang).includes(value)
      ? value
      : this.defaultLang;

    this.subjLang.next(value);

    if (this.appCommonStorage && this.appCommonStorage.lang !== value) {
      this.appCommonStorage.lang = value;
    }
  }

  get prevUsername(): string {
    return this.appCommonStorage.prevUsername;
  }

  set prevUsername(value: string) {
    this.appCommonStorage.prevUsername = value;
  }

  i18n(propName: string, langMap: any): string {
    if (langMap) {
      return this.i18nExtract(langMap[propName]);
    }
    return 'UNDEFINED LANG MAP';
  }

  i18nExtract(prop: any): string {
    let result = 'UNDEFINED LANG PROP';
    if (prop) {
      if (prop[this.lang]) {
        result = prop[this.lang];
      } else if (prop[this.defaultLang]) {
        result = prop[this.defaultLang];
      }
    }
    return result;
  }
}
