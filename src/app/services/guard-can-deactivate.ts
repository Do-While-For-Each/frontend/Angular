import {Injectable} from '@angular/core';
import {CanDeactivate} from '@angular/router';

import {DwfeCanComponentDeactivate} from '@dwfe/classes/DwfeCanComponentDeactivate';

@Injectable()
export class AppGuardCanDeactivate implements CanDeactivate<DwfeCanComponentDeactivate> {

  canDeactivate(component: DwfeCanComponentDeactivate) {
    return component && component.canDeactivate ? component.canDeactivate() : true;
  }
}
