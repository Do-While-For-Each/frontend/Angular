import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {MatTabsModule} from '@angular/material';

import {DwfeRoutedTabModule} from '@dwfe/directives/routed-tab/routed-tabs.module';

import {NevisAccountModule} from '@nevis/components/account/account.module';
import {NevisAccountComponent} from '@nevis/components/account/account.component';
import {NevisAccountPersonalComponent} from '@nevis/components/account/personal/personal.component';
import {NevisAccountEmailComponent} from '@nevis/components/account/email/email.component';
import {NevisAccountPhoneComponent} from '@nevis/components/account/phone/phone.component';
import {NevisAccountPasswordNDeleteComponent} from '@nevis/components/account/password-n-delete/password-n-delete.component';

import {AppLogoHeaderModule} from '../../components/elements/logo-header/logo-header.module';

import {PageSettingsModule} from '../../components/pages/settings/settings.module';
import {PageSettingsComponent} from '../../components/pages/settings/settings.component';

import {AppGuardCanDeactivate} from '../../services/guard-can-deactivate';

import {AppLayoutControlPanelComponent} from './control-panel.component';

@NgModule({
  declarations: [
    AppLayoutControlPanelComponent
  ],
  imports: [
    CommonModule,

    MatTabsModule,

    AppLogoHeaderModule,

    PageSettingsModule,

    DwfeRoutedTabModule,
    NevisAccountModule,

    RouterModule.forChild([
      {
        path: '', component: AppLayoutControlPanelComponent, children: [
          {path: '', pathMatch: 'full', redirectTo: 'account'},
          {
            path: 'account', component: NevisAccountComponent, children: [
              {path: '', pathMatch: 'full', redirectTo: 'personal'},
              {path: 'personal', canDeactivate: [AppGuardCanDeactivate], component: NevisAccountPersonalComponent},
              {path: 'email', canDeactivate: [AppGuardCanDeactivate], component: NevisAccountEmailComponent},
              {path: 'phone', canDeactivate: [AppGuardCanDeactivate], component: NevisAccountPhoneComponent},
              {path: 'password', component: NevisAccountPasswordNDeleteComponent},
            ]
          },
          {path: 'settings', component: PageSettingsComponent},
        ]
      },
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppLayoutControlPanelModule {
}
