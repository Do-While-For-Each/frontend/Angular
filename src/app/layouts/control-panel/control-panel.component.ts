import {Component, ViewEncapsulation} from '@angular/core';

import {AppService} from '../../services/app.service';

import {T7E_APP_LAYOUT_CONTROL_PANEL} from './i18n';

@Component({
  selector: 'app-layout-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppLayoutControlPanelComponent {

  constructor(public app: AppService) {
  }

  get i18n(): any {
    return T7E_APP_LAYOUT_CONTROL_PANEL;
  }
}
