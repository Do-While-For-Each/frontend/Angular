export const T7E_APP_LAYOUT_AUTH = {
  login: {
    EN: 'Log in',
    RU: 'Вход',
  },
  create_account: {
    EN: 'Create account',
    RU: 'Создать аккаунт',
  },
  terms: {
    EN: 'Terms of use',
    RU: 'Условия',
  },
  privacy: {
    EN: 'Privacy policy',
    RU: 'Конфиденциальность',
  },
};
