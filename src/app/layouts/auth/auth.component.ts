import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {AppService} from '../../services/app.service';

import {T7E_APP_LAYOUT_AUTH} from './i18n';

@Component({
  selector: 'app-layout-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppLayoutAuthComponent implements OnInit, OnDestroy {

  navLinks: { path: string, label: string }[];

  subjLatchForUnsubscribe = new Subject();

  constructor(public router: Router,
              public app: AppService) {
  }

  ngOnInit(): void {
    this.app
      .lang$
      .pipe(
        takeUntil(this.subjLatchForUnsubscribe.asObservable())
      )
      .subscribe(
        () => {
          this.navLinks = [
            {path: 'login', label: this.app.i18n('login', this.i18n)},
            {path: 'create-account', label: this.app.i18n('create_account', this.i18n)},
          ];
        });
  }

  ngOnDestroy(): void {
    this.subjLatchForUnsubscribe.next();
  }

  get i18n(): any {
    return T7E_APP_LAYOUT_AUTH;
  }
}
