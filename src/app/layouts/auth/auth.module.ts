import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {MatTabsModule} from '@angular/material';

import {NevisAuthModule} from '@nevis/components/auth/auth.module';
import {NevisAuthCreateAccountComponent} from '@nevis/components/auth/create-account/create-account.component';
import {NevisAuthLoginComponent} from '@nevis/components/auth/login/login.component';

import {AppLanguageModule} from '../../components/elements/language/language.module';
import {AppLogoHeaderModule} from '../../components/elements/logo-header/logo-header.module';

import {AppLayoutAuthComponent} from './auth.component';

@NgModule({
  declarations: [
    AppLayoutAuthComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,

    MatTabsModule,

    AppLanguageModule,
    AppLogoHeaderModule,

    NevisAuthModule,

    RouterModule.forChild([
      {
        path: '', component: AppLayoutAuthComponent, children: [
          {path: '', pathMatch: 'full', redirectTo: 'login'},
          {path: 'create-account', component: NevisAuthCreateAccountComponent},
          {path: 'login', component: NevisAuthLoginComponent},
        ]
      }
    ])
  ],
  exports: [
    RouterModule
  ],
})
export class AppLayoutAuthModule {
}
