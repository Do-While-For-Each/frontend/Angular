import {Component, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-layout-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppLayoutSiteComponent {
}
