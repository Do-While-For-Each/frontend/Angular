import {NgModule} from '@angular/core';

import {DwfeRoutedTabDirective} from './routed-tab.directive';
import {DwfeRoutedTabsDirective} from './routed-tabs.directive';

@NgModule({
  declarations: [
    DwfeRoutedTabDirective,
    DwfeRoutedTabsDirective,
  ],
  imports: [],
  exports: [
    DwfeRoutedTabDirective,
    DwfeRoutedTabsDirective,
  ],
})
export class DwfeRoutedTabModule {
}
