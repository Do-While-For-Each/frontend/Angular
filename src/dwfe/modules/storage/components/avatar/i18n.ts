export const T7E_STORAGE_AVATAR = {

  upload: {
    EN: 'Upload',
    RU: 'Загрузить',
  },

  successfully_uploaded: {
    EN: 'Avatar has been successfully updated',
    RU: 'Аватар успешно обновлен',
  },

  // : {
  //   EN: '',
  //   RU: '',
  // },

};
