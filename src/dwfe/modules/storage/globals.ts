const PREFIX = '/STORAGE/';

export const DWFE_STORAGE_ENDPOINTS = {
  uploadByUserNevis: `${PREFIX}upload-by-user/nevis`,
};
