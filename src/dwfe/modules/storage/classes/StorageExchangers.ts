import {Observable} from 'rxjs';

import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractExchanger} from '@dwfe/classes/DwfeAbstractExchanger';

import {DWFE_STORAGE_ENDPOINTS} from '../globals';


export class UploadByUserNevisExchanger extends DwfeAbstractExchanger {
  getHttpReq$(params?: any): Observable<Object> {
    return this.http.post(
      DWFE_STORAGE_ENDPOINTS.uploadByUserNevis,
      params.formData,
      DwfeGlobals.optForMultipartFormDataAuthorizedReq(this.accessToken));
  }
}
