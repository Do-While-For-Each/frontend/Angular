export const DWFE_STORAGE_ERROR_CODES_MAP = {
  'invalid-directory': {
    EN: 'Invalid directory',
    RU: 'Некорректная директория',
  },

  'invalid-filename': {
    EN: 'File name error',
    RU: 'Ошибка в имени файла',
  },

  // '': {
  //   EN: '',
  //   RU: '',
  // },
};
