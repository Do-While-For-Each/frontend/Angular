import {HttpClientModule} from '@angular/common/http';
import {inject, TestBed} from '@angular/core/testing';

import {RouterTestingModule} from '@angular/router/testing';

import {DwfeNevisService} from './nevis.service';

const globals = require('@dwfe/globalsForTests');

describe('DwfeNevisService', () => {

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule
      ],
      providers: [
        DwfeNevisService,
      ]
    });
  });


  it('DwfeNevisService',

    inject([DwfeNevisService], async (nevisService: DwfeNevisService) => {

      const nevisStorageKey = 'nevisAccountAccess';

      expect(nevisService).toBeTruthy();
      expect(nevisService.auth).toBeFalsy();
      expect(localStorage.getItem(nevisStorageKey)).toBeFalsy();

      nevisService.performLoginStandard(globals.testUserName, globals.testPassword, 'TRUSTED');

      await globals.delay(1000);

      expect(nevisService.auth).toBeTruthy();
      expect(localStorage.getItem(nevisStorageKey)).toBeTruthy();

      expect(nevisService.auth.access_token.length).toEqual(36);

      nevisService.logout();
      await globals.delay(500);

      expect(localStorage.getItem(nevisStorageKey)).toBeFalsy();
    }));

});
