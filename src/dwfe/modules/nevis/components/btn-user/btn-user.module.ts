import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {MatButtonModule} from '@angular/material';

import {NevisBtnUserComponent} from './btn-user.component';
import {NevisBtnUserNotAuthenticatedComponent} from './not-authenticated/not-authenticated.component';
import {NevisBtnUserLoggedInComponent} from './logged-in/logged-in.component';

@NgModule({
  declarations: [
    NevisBtnUserComponent,
    NevisBtnUserNotAuthenticatedComponent,
    NevisBtnUserLoggedInComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,

    MatButtonModule,
  ],
  exports: [
    NevisBtnUserComponent,
  ],
})
export class NevisBtnUserModule {
}
