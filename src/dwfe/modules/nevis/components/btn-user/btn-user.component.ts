import {Component, ViewEncapsulation} from '@angular/core';

import {DwfeNevisService} from '../../services/nevis.service';

@Component({
  selector: 'nevis-btn-user',
  templateUrl: './btn-user.component.html',
  styleUrls: ['./btn-user.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisBtnUserComponent {

  constructor(public nevisService: DwfeNevisService) {
  }
}
