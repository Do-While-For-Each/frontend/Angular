import {Component, ViewEncapsulation} from '@angular/core';

import {AppService} from '@app/services/app.service';

import {I18N_NEVIS_ACCOUNT} from './i18n';

@Component({
  selector: 'nevis-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisAccountComponent {

  constructor(public app: AppService) {
  }

  get i18n(): any {
    return I18N_NEVIS_ACCOUNT;
  }
}
