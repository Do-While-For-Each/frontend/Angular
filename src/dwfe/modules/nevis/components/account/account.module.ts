import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {MatDividerModule, MatTabsModule} from '@angular/material';

import {DwfeActionOkModule} from '@dwfe/components/action/ok/action-ok.module';
import {DwfeActionOkCancelModule} from '@dwfe/components/action/ok-cancel/action-ok-cancel.module';
import {DwfeFormControlDatePickerModule} from '@dwfe/components/form-control/date-picker/date-picker.module';
import {DwfeFormControlInputEmailModule} from '@dwfe/components/form-control/input-email/input-email.module';
import {DwfeFormControlInputTextModule} from '@dwfe/components/form-control/input-text/input-text.module';
import {DwfeFormControlInputPasswordModule} from '@dwfe/components/form-control/input-password/input-password.module';
import {DwfeFormControlSelectModule} from '@dwfe/components/form-control/select/select.module';
import {DwfeFormControlToggleModule} from '@dwfe/components/form-control/toggle/toggle.module';
import {DwfeOverlayModule} from '@dwfe/components/overlay/overlay.module';
import {DwfeRoutedTabModule} from '@dwfe/directives/routed-tab/routed-tabs.module';

import {DwfeStorageAvatarModule} from '@storage/components/avatar/avatar.module';

import {NevisAccountComponent} from './account.component';
import {NevisAccountEmailComponent} from './email/email.component';
import {NevisAccountPasswordNDeleteComponent} from './password-n-delete/password-n-delete.component';
import {NevisAccountPersonalComponent} from './personal/personal.component';
import {NevisAccountPhoneComponent} from './phone/phone.component';

@NgModule({
  declarations: [
    NevisAccountComponent,
    NevisAccountEmailComponent,
    NevisAccountPasswordNDeleteComponent,
    NevisAccountPersonalComponent,
    NevisAccountPhoneComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,

    MatDividerModule,
    MatTabsModule,

    DwfeActionOkModule,
    DwfeActionOkCancelModule,
    DwfeFormControlDatePickerModule,
    DwfeFormControlInputEmailModule,
    DwfeFormControlInputTextModule,
    DwfeFormControlInputPasswordModule,
    DwfeFormControlSelectModule,
    DwfeFormControlToggleModule,
    DwfeOverlayModule,
    DwfeRoutedTabModule,

    DwfeStorageAvatarModule,
  ],
  exports: [
    NevisAccountComponent,
  ]
})
export class NevisAccountModule {
}
