import {AfterViewInit, Component} from '@angular/core';
import {AbstractControl} from '@angular/forms';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';
import {DwfeCanComponentDeactivate} from '@dwfe/classes/DwfeCanComponentDeactivate';

import {I18N_NEVIS_ACCOUNT_PHONE} from '../i18n';
import {DwfeNevisService} from '../../../services/nevis.service';
import {NevisAccountPhoneBackendEntity} from '../../../classes/NevisAccountPhoneBackendEntity';

@Component({
  selector: 'nevis-account-phone',
  templateUrl: './phone.component.html',
  styleUrls: ['./phone.component.scss']
})
export class NevisAccountPhoneComponent extends DwfeAbstractExchangeableComponent implements AfterViewInit, DwfeCanComponentDeactivate {

  subjCancelEdit_Info = new Subject<boolean>();
  subjSaveEdit_Info = new Subject<boolean>();
  subjSaveEdit_Change = new Subject<boolean>();

  cCurrentPassword: AbstractControl;

  cPhone: AbstractControl;
  cNewPhone: AbstractControl;
  tPhone: AbstractControl;
  newPhone_changed: boolean;
  phoneNonPublic_changed: boolean;

  constructor(public nevisService: DwfeNevisService,
              public app: AppService) {
    super();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {

      this.nevisService
        .accountPhone$({
          'initiator': this,
          'responseHandlerFn':
            (data: DwfeExchangeResult) => {
              if (data.result) {
                this.nevisService.accountPhone = data.data;
              } else {
                this.cPhone.setValue(null);
                this.tPhone.setValue(false);
                this.cNewPhone.setValue(null);
                this.setErrorMessage(data.description);
              }
            }
        })
        .pipe(
          takeUntil(this.latchForUnsubscribe$)
        )
        .subscribe(
          (aPhone: NevisAccountPhoneBackendEntity) => {
            this.cPhone.setValue(aPhone.value);

            this.cNewPhone.setValue(aPhone.value);
            this.cNewPhone.markAsUntouched();
            this.cNewPhone.markAsPristine();

            this.tPhone.setValue(!aPhone.nonPublic);
          });

      this.cPhone.disable();

      this.resetMessage(this.cCurrentPassword, ['errorMessage', 'successMessage']);

      this.resetMessage(this.tPhone, ['errorMessage', 'successMessage']);
      this.resetMessage(this.cNewPhone, ['errorMessage', 'successMessage']);

    }); // to prevent ExpressionChangedAfterItHasBeenCheckedError
  }

  get isInfoChanged(): boolean {
    return this.phoneNonPublic_changed;
  }

  performCancelEdit() {
    this.subjCancelEdit_Info.next(true);
  }

  performUpdateAccountPhone() {
    this.activeExchangeUnitNumber = 1;

    this.nevisService
      .updateAccountPhoneExchanger
      .run(this,
        this.getUpdateAccountPhoneRequestBody(),
        (data: DwfeExchangeResult) => {
          if (data.result) {
            this.nevisService.updateAccountPhone({nonPublic: !this.tPhone.value});
            this.subjSaveEdit_Info.next(true);
            this.successMessage = this.app.i18n('info_success_updated', this.i18n);
          } else {
            this.setErrorMessage(data.description);
          }
        });
  }

  getUpdateAccountPhoneRequestBody(): string {
    const req = {};

    if (this.phoneNonPublic_changed) {
      req['nonPublic'] = !this.tPhone.value;
    }

    return DwfeGlobals.prepareReq(req);
  }

  performChangePhone() {
    this.activeExchangeUnitNumber = 2;
    const newphone = this.cNewPhone.value;

    this.nevisService
      .phoneChangeExchanger
      .run(this,
        DwfeGlobals.prepareReq({
          newphone: newphone,
          curpass: this.cCurrentPassword.value
        }),
        (data: DwfeExchangeResult) => {
          this.cCurrentPassword.reset();
          if (data.result) {
            this.nevisService.updateAccountPhone({value: newphone});
            this.subjSaveEdit_Change.next(true);
            this.successMessage = this.app.i18n('phone_success_changed', this.i18n);
          } else {
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (this.isInfoChanged || this.isLocked) {
      return confirm(this.app.i18n('save_changes', this.i18n));
    } else {
      return true;
    }
  }

  get i18n(): any {
    return I18N_NEVIS_ACCOUNT_PHONE;
  }
}
