import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {MatButtonModule, MatDividerModule} from '@angular/material';

import {DwfeOverlayModule} from '@dwfe/components/overlay/overlay.module';
import {DwfePreviewPicCircledModule} from '@dwfe/components/preview-pic-circled/preview-pic-circled.module';

import {NevisAccountPublicInfoComponent} from './public-info.component';

@NgModule({
  declarations: [
    NevisAccountPublicInfoComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDividerModule,
    DwfeOverlayModule,
    DwfePreviewPicCircledModule,

    RouterModule.forChild([
      {path: '', component: NevisAccountPublicInfoComponent}
    ])
  ],
  exports: [
    RouterModule,
  ],
})
export class NevisAccountPublicInfoModule {
}
