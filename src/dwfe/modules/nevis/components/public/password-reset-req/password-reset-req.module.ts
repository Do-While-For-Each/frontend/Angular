import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {DwfeCommonGoogleCaptchaModule} from '@common/components/google-captcha/google-captcha.module';

import {DwfeActionOkModule} from '@dwfe/components/action/ok/action-ok.module';
import {DwfeFormControlInputEmailModule} from '@dwfe/components/form-control/input-email/input-email.module';
import {DwfeOverlayModule} from '@dwfe/components/overlay/overlay.module';

import {NevisPasswordResetReqComponent} from './password-reset-req.component';

@NgModule({
  declarations: [
    NevisPasswordResetReqComponent,
  ],
  imports: [
    CommonModule,
    DwfeCommonGoogleCaptchaModule,
    DwfeActionOkModule,
    DwfeFormControlInputEmailModule,
    DwfeOverlayModule,

    RouterModule.forChild([
      {path: '', component: NevisPasswordResetReqComponent}
    ])
  ],
  exports: [
    RouterModule
  ],
})
export class NevisPasswordResetReqModule {
}
