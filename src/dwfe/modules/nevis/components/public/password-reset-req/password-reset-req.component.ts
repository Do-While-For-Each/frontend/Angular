import {AfterViewInit, Component, ElementRef, HostListener, ViewChild, ViewEncapsulation} from '@angular/core';
import {AbstractControl} from '@angular/forms';

import {of} from 'rxjs';
import {concatMap, delay, takeUntil} from 'rxjs/operators';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals, FocusOnType} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {DwfeCommonService} from '@common/services/common.service';

import {DwfeNevisService} from '../../../services/nevis.service';
import {I18N_NEVIS_PASSWORD_RESET_REQ} from './i18n';

@Component({
  selector: 'nevis-password-reset-req',
  templateUrl: './password-reset-req.component.html',
  styleUrls: ['./password-reset-req.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisPasswordResetReqComponent extends DwfeAbstractExchangeableComponent implements AfterViewInit {

  cEmail: AbstractControl;
  @ViewChild('refEmail', {read: ElementRef}) refEmail: ElementRef;

  isExchangeCompleted = false;

  constructor(public nevisService: DwfeNevisService,
              public commonService: DwfeCommonService,
              public app: AppService) {
    super();
  }

  ngAfterViewInit(): void {

    this
      .isCaptchaValid$
      .pipe(
        takeUntil(this.latchForUnsubscribe$),
        concatMap(x => of(x)
          .pipe(
            delay(10))) // otherwise below cEmail return undefined
      ).subscribe(
      isCaptchaValid => {
        if (isCaptchaValid) {
          this.resetMessage(this.cEmail, ['errorMessage']);
          this.focusOn(this.refEmail, FocusOnType.DWFE_INPUT);
        } else {
          this.isExchangeCompleted = false;
        }
      });
  }

  setLocked(value: boolean): void {
    super.setLocked(value);
    if (!value && this.refEmail) {
      this.focusOn(this.refEmail, FocusOnType.DWFE_INPUT);
    }
  }

  performPasswordResetReq() {

    this.nevisService
      .passwordResetReqExchanger
      .run(this,
        DwfeGlobals.prepareReq({
          email: this.cEmail.value
        }),
        (data: DwfeExchangeResult) => {
          if (data.result) {    // actions on success 'Password Reset Request'
            this.isExchangeCompleted = true;
          } else {
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  isDisabledBtnPerformPasswordResetReq(): boolean {
    return !(this.cEmail && this.cEmail.valid) || this.isLocked;
  }

  @HostListener('document:keyup', ['$event'])
  onKeyUp(evt: KeyboardEvent) {

    if (!this.isDisabledBtnPerformPasswordResetReq()
      && DwfeGlobals.isEnterKey(evt)) {
      this.performPasswordResetReq();
    }
  }

  get i18n(): any {
    return I18N_NEVIS_PASSWORD_RESET_REQ;
  }
}
