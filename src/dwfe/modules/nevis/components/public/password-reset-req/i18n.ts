export const I18N_NEVIS_PASSWORD_RESET_REQ = {
  title_reset_req: {
    EN: 'Password reset request',
    RU: 'Запрос на сброс пароля',
  },
  captcha_title: {
    EN: 'Please confirm that you are a real person:',
    RU: 'Подтвердите, что вы человек:',
  },
  email_associated_with_account: {
    EN: 'E-mail associated with your account',
    RU: 'Эл. почта вашего аккаунта',
  },
  successfully_completed: {
    EN: 'Successfully completed',
    RU: 'Успешно выполнен',
  },
  check_email_box: {
    EN: 'Please check your e-mail box',
    RU: 'Пожалуйста проверьте свой почтовый ящик',
  },
};
