import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {DwfeOverlayModule} from '@dwfe/components/overlay/overlay.module';

import {NevisEmailConfirmComponent} from './email-confirm.component';

@NgModule({
  declarations: [
    NevisEmailConfirmComponent,
  ],
  imports: [
    CommonModule,
    DwfeOverlayModule,

    RouterModule.forChild([
      {path: '', component: NevisEmailConfirmComponent}
    ])
  ],
  exports: [
    RouterModule,
  ],
})
export class NevisEmailConfirmModule {
}
