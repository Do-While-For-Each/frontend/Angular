export const I18N_NEVIS_EMAIL_CONFIRM = {
  title: {
    EN: 'E-mail confirm',
    RU: 'Подтверждение эл. почты',
  },
  error: {
    EN: 'Error',
    RU: 'Ошибка',
  },
  email_already_confirmed: {
    EN: 'Your e-mail is already confirmed',
    RU: 'Ваша эл. почта уже подтверждена',
  },
  email_now_confirmed: {
    EN: 'Your e-mail is now confirmed',
    RU: 'Теперь ваша эл. почта подтверждена',
  },
};
