import {AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {AbstractControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals, FocusOnType} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {DwfeNevisService} from '../../../services/nevis.service';
import {I18N_NEVIS_PASSWORD_RESET} from './i18n';

@Component({
  selector: 'nevis-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisPasswordResetComponent extends DwfeAbstractExchangeableComponent implements OnInit, AfterViewInit {

  confirmKey: string;

  cNewPassword: AbstractControl;
  cRepeatPassword: AbstractControl;

  @ViewChild('refNewPassword', {read: ElementRef}) refNewPassword: ElementRef;
  @ViewChild('refRepeatPassword', {read: ElementRef}) refRepeatPassword: ElementRef;

  isExchangeCompleted = false;
  username: string;

  constructor(public nevisService: DwfeNevisService,
              public activatedRoute: ActivatedRoute,
              public router: Router,
              public app: AppService) {
    super();
  }

  ngOnInit(): void {
    this.confirmKey = this.activatedRoute.snapshot.paramMap.get('confirmKey');
  }

  ngAfterViewInit(): void {
    this.resetMessage(this.cNewPassword, ['errorMessage']);
    this.resetMessage(this.cRepeatPassword, ['errorMessage']);

    setTimeout(() => {
      this.focus();
    }); // to prevent ExpressionChangedAfterItHasBeenCheckedError
  }

  setLocked(value: boolean): void {
    super.setLocked(value);
    if (!value) {
      this.focus();
    }
  }

  focus() {
    if (this.cNewPassword.invalid) {
      this.focusOn(this.refNewPassword, FocusOnType.DWFE_INPUT);
    } else if (this.cRepeatPassword.invalid) {
      this.focusOn(this.refRepeatPassword, FocusOnType.DWFE_INPUT);
    }
  }

  performPasswordReset() {

    if (this.cNewPassword.value !== this.cRepeatPassword.value) {
      this.setErrorMessage(this.app.i18n('different_passwords', this.i18n));
      return;
    }

    this.nevisService
      .passwordResetExchanger
      .run(this,
        DwfeGlobals.prepareReq({
          key: this.confirmKey,
          newpass: this.cNewPassword.value
        }),
        (data: DwfeExchangeResult) => {
          if (data.result) {    // actions on success 'Password Reset'
            this.isExchangeCompleted = true;
            this.username = data.data.username;
          } else {
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  goToLoginPage() {

    this.router.navigate(
      ['/auth/login'], {
        queryParams: {
          username: this.username
        }
      });
  }

  isDisabledBtnPerformPasswordReset(): boolean {
    return !(this.cNewPassword && this.cRepeatPassword && this.cNewPassword.valid && this.cRepeatPassword.valid) || this.isLocked;
  }

  @HostListener('document:keyup', ['$event'])
  onKeyUp(evt: KeyboardEvent) {

    if (DwfeGlobals.isEnterKey(evt)) {

      if (this.isExchangeCompleted) {
        this.goToLoginPage();
      } else {
        if (!this.isDisabledBtnPerformPasswordReset()) {
          this.performPasswordReset();
        }
      }
    }
  }

  get i18n(): any {
    return I18N_NEVIS_PASSWORD_RESET;
  }
}
