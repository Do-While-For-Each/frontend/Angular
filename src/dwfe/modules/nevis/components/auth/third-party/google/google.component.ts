import {ApplicationRef, Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';

import {environment} from '@environment/environment';

import {DwfeGlobals} from '@dwfe/globals';

declare let gapi; // will appear in the DOM after loading <script>

@Component({
  selector: 'nevis-auth-google',
  templateUrl: './google.component.html',
  styleUrls: ['./google.component.scss']
})
export class NevisAuthGoogleComponent implements OnInit {

  googleAuth;

  isDisabled = true;

  @Output() signInResult = new EventEmitter<any>();

  @ViewChild('refActionBtn', {read: ElementRef}) refActionBtn: ElementRef;

  constructor(public appRef: ApplicationRef) {
  }

  ngOnInit() {
    DwfeGlobals.appendChildScript(
      '//apis.google.com/js/platform.js',
      true,
      document.getElementsByTagName('nevis-auth-google').item(0),
      () => {
        gapi
          .load('auth2', () => { // https://developers.google.com/identity/sign-in/web/reference#auth-setup
            gapi.auth2
              .init({                // https://developers.google.com/identity/sign-in/web/reference#gapiauth2initparams
                ...environment.NEVIS.THIRD_PARTY.GOOGLE
              })
              .then(                 // https://developers.google.com/identity/sign-in/web/reference#googleauththenoninit-onerror
                googleAuth => {
                  this.googleAuth = googleAuth; // now GogleAuth is fully initialized

                  this.isDisabled = false;

                  // DwfeGlobals.triggerChangeDetection();
                  this.appRef.tick(); // because 'triggerChangeDetection' is unstable for google auth... strangely
                },
                error => {
                  // on init error 'isDisabled' still remains equals to 'true'
                });
          });
      }
    );
    this.signInResult.emit(null);
  }

  performSignInOnGoogleServer() {
    this.signInResult.emit(null);
    DwfeGlobals.getNativeElementFromRef(this.refActionBtn).blur();

    const signInResult = this.googleAuth.signIn(); // https://developers.google.com/identity/sign-in/web/reference#googleauthsignin

    signInResult.then(
      googleUser => {
        this.signInResult.emit({
            thirdParty: this,
            req: {
              identityCheckData: googleUser.getAuthResponse().id_token, // https://developers.google.com/identity/sign-in/web/backend-auth#send-the-id-token-to-your-server
              thirdParty: 'GOOGLE',
              email: googleUser.getBasicProfile().getEmail(),
            }
          }
        );
      },
      error => {
      });
  }

  performSignOut() {
    this.googleAuth.signOut();
  }
}
