import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {DwfeGlobals} from '@dwfe/globals';

@Component({
  selector: 'nevis-auth-twitter',
  templateUrl: './twitter.component.html',
  styleUrls: ['./twitter.component.scss']
})
export class NevisAuthTwitterComponent implements OnInit {

  isDisabled = true;

  @Output() signInResult = new EventEmitter<any>();

  @ViewChild('refActionBtn', {read: ElementRef}) refActionBtn: ElementRef;

  ngOnInit() {
    this.signInResult.emit(null);
  }

  performSignInOnTwitterServer() {
    this.signInResult.emit(null);
    DwfeGlobals.getNativeElementFromRef(this.refActionBtn).blur();
  }

  performSignOut() {
  }
}
