import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';

import {environment} from '@environment/environment';

import {DwfeGlobals} from '@dwfe/globals';

declare let FB; // will appear in the DOM after loading <script>

@Component({
  selector: 'nevis-auth-facebook',
  templateUrl: './facebook.component.html',
  styleUrls: ['./facebook.component.scss']
})
export class NevisAuthFacebookComponent implements OnInit {

  isDisabled = true;

  @Output() signInResult = new EventEmitter<any>();

  @ViewChild('refActionBtn', {read: ElementRef}) refActionBtn: ElementRef;

  ngOnInit() {
    DwfeGlobals.appendChildScript(
      '//connect.facebook.net/en_US/sdk.js', // https://developers.facebook.com/docs/javascript/quickstart
      false,
      document.getElementsByTagName('nevis-auth-facebook').item(0),
      () => {
        FB.init({ // https://developers.facebook.com/docs/facebook-login/web#example
          ...environment.NEVIS.THIRD_PARTY.FACEBOOK,
          cookie: false,  // enable cookies to allow the server to access the session
          xfbml: false,   // parse social plugins on this page
        });

        this.isDisabled = false;
        DwfeGlobals.triggerChangeDetection();
      }
    );
    this.signInResult.emit(null);
  }

  performSignInOnFacebookServer() {
    this.signInResult.emit(null);
    DwfeGlobals.getNativeElementFromRef(this.refActionBtn).blur();

    FB.login(
      respLogin => { // https://developers.facebook.com/docs/facebook-login/web#dialogresponse
        if (respLogin.status === 'connected') { // Logged into your app and Facebook
          FB.api('/me?fields=email',
            respApi => {
              this.signInResult.emit({
                thirdParty: this,
                req: {
                  identityCheckData: respLogin.authResponse.accessToken, // https://developers.facebook.com/docs/facebook-login/web#checklogin
                  thirdParty: 'FACEBOOK',
                  email: respApi.email,
                }
              });
            });
        }
      }, {
        scope: 'email',        // https://developers.facebook.com/docs/facebook-login/web/permissions
        auth_type: 'rerequest'
      }
    );
  }

  performSignOut() {
    FB.logout();
  }
}
