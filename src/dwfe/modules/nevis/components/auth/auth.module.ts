import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MatButtonModule, MatTooltipModule} from '@angular/material';

import {DwfeCommonGoogleCaptchaModule} from '@common/components/google-captcha/google-captcha.module';

import {DwfeActionOkModule} from '@dwfe/components/action/ok/action-ok.module';
import {DwfeAlertModule} from '@dwfe/components/alert/alert.module';
import {DwfeDialogSimpleModule} from '@dwfe/components/dialog/simple/dialog-simple.module';
import {DwfeFormControlCheckboxModule} from '@dwfe/components/form-control/checkbox/checkbox.module';
import {DwfeFormControlInputEmailModule} from '@dwfe/components/form-control/input-email/input-email.module';
import {DwfeFormControlInputPasswordModule} from '@dwfe/components/form-control/input-password/input-password.module';
import {DwfeOverlayModule} from '@dwfe/components/overlay/overlay.module';

import {NevisAuthCreateAccountComponent} from './create-account/create-account.component';
import {NevisAuthLoginComponent} from './login/login.component';

import {NevisAuthThirdPartyComponent} from './third-party/third-party.component';
import {NevisAuthGoogleComponent} from './third-party/google/google.component';
import {NevisAuthFacebookComponent} from './third-party/facebook/facebook.component';
import {NevisAuthVkontakteComponent} from './third-party/vkontakte/vkontakte.component';
import {NevisAuthTwitterComponent} from './third-party/twitter/twitter.component';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    NevisAuthCreateAccountComponent,
    NevisAuthLoginComponent,

    NevisAuthThirdPartyComponent,
    NevisAuthGoogleComponent,
    NevisAuthFacebookComponent,
    NevisAuthVkontakteComponent,
    NevisAuthTwitterComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,

    MatButtonModule,
    MatTooltipModule,

    DwfeCommonGoogleCaptchaModule,

    DwfeActionOkModule,
    DwfeAlertModule,
    DwfeDialogSimpleModule,
    DwfeFormControlCheckboxModule,
    DwfeFormControlInputEmailModule,
    DwfeFormControlInputPasswordModule,
    DwfeOverlayModule,
  ],
  exports: [
    NevisAuthCreateAccountComponent,
    NevisAuthLoginComponent,
  ]
})
export class NevisAuthModule {
}
