const PREFIX = '/NEVIS/';

export const NEVIS_ENDPOINTS = {
  signIn: `${PREFIX}sign-in`,
  tokenRefresh: `${PREFIX}sign-in`,
  signOut: `${PREFIX}sign-out`,

  canUseUsername: `${PREFIX}can-use-username`,
  canUsePassword: `${PREFIX}can-use-password`,
  createAccount: `${PREFIX}create-account`,
  id: `${PREFIX}id`,
  deleteAccount: `${PREFIX}delete-account`,
  thirdPartyAuth: `${PREFIX}third-party-auth`,

  passwordChange: `${PREFIX}password-change`,
  passwordResetReq: `${PREFIX}password-reset-req`,
  passwordReset: `${PREFIX}password-reset`,
  setAvatarUrl: `${PREFIX}set-avatar-url`,

  getAccountEmail: `${PREFIX}get-account-email`,
  emailConfirmReq: `${PREFIX}email-confirm-req`,
  emailConfirm: `${PREFIX}email-confirm`,
  emailChange: `${PREFIX}email-change`,
  updateAccountEmail: `${PREFIX}update-account-email`,

  getAccountPhone: `${PREFIX}get-account-phone`,
  phoneChange: `${PREFIX}phone-change`,
  updateAccountPhone: `${PREFIX}update-account-phone`,

  getAccountPersonal: `${PREFIX}get-account-personal`,
  nickNameChange: `${PREFIX}nickname-change`,
  updateAccountPersonal: `${PREFIX}update-account-personal`,
};

export enum NevisClientType {
  UNTRUSTED = 'UNTRUSTED',
  TRUSTED = 'TRUSTED',
  UNLIMITED = 'UNLIMITED',
}

const NEVIS_CLIENT_BACKEND_CREDENTIALS = {
  untrusted: { // the access_token is issued for a very short time, e.g. 3 minutes
    name: 'untrusted',
    password: 'untrusted'
  },
  trusted: {   // issued access_token is valid for a long time, e.g. 20 days
    name: 'trusted',
    password: 'trusted'
  },
  unlimited: {   // issued access_token is valid forever
    name: 'unlimited',
    password: 'unlimited'
  },
};

const CLIENT_CREDENTIALS_BASE64_ENCODED = {
  untrusted: 'Basic ' + btoa(NEVIS_CLIENT_BACKEND_CREDENTIALS.untrusted.name + ':' + NEVIS_CLIENT_BACKEND_CREDENTIALS.untrusted.password),
  trusted: 'Basic ' + btoa(NEVIS_CLIENT_BACKEND_CREDENTIALS.trusted.name + ':' + NEVIS_CLIENT_BACKEND_CREDENTIALS.trusted.password),
  unlimited: 'Basic ' + btoa(NEVIS_CLIENT_BACKEND_CREDENTIALS.unlimited.name + ':' + NEVIS_CLIENT_BACKEND_CREDENTIALS.unlimited.password),
};

export const NEVIS_OPT_FOR_UNTRUSTED_CLIENT = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': CLIENT_CREDENTIALS_BASE64_ENCODED.untrusted
  }
};

export const NEVIS_OPT_FOR_TRUSTED_CLIENT = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': CLIENT_CREDENTIALS_BASE64_ENCODED.trusted
  }
};

export const NEVIS_OPT_FOR_UNLIMITED_CLIENT = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': CLIENT_CREDENTIALS_BASE64_ENCODED.unlimited
  }
};

export class NevisGlobals {
}
