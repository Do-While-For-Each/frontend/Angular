import {DwfeAbstractStorageEntity} from '@dwfe/classes/DwfeAbstractStorageEntity';

import {NevisAccountPersonalModel} from '../models/NevisAccountPersonalModel';

export class NevisAccountPersonalBackendEntity extends DwfeAbstractStorageEntity implements NevisAccountPersonalModel {

  nickName;
  nickNameNonPublic;

  firstName;
  firstNameNonPublic;

  middleName;
  middleNameNonPublic;

  lastName;
  lastNameNonPublic;

  gender;
  genderNonPublic;

  dateOfBirth;
  dateOfBirthNonPublic;

  country;
  countryNonPublic;

  city;
  cityNonPublic;

  company;
  companyNonPublic;

  positionHeld;
  positionHeldNonPublic;

  updatedOn;

  get storageKey(): string {
    return 'nevisAccountPersonal';
  }

  static of(personalResp: NevisAccountPersonalModel, isOAuthClientUntrusted: boolean): NevisAccountPersonalBackendEntity {
    const obj = new NevisAccountPersonalBackendEntity();

    obj.nickName = personalResp.nickName;
    obj.nickNameNonPublic = personalResp.nickNameNonPublic;

    obj.firstName = personalResp.firstName;
    obj.firstNameNonPublic = personalResp.firstNameNonPublic;

    obj.middleName = personalResp.middleName;
    obj.middleNameNonPublic = personalResp.middleNameNonPublic;

    obj.lastName = personalResp.lastName;
    obj.lastNameNonPublic = personalResp.lastNameNonPublic;

    obj.gender = personalResp.gender;
    obj.genderNonPublic = personalResp.genderNonPublic;

    obj.dateOfBirth = obj.dateFromObj(personalResp, 'dateOfBirth');
    obj.dateOfBirthNonPublic = personalResp.dateOfBirthNonPublic;

    obj.country = personalResp.country;
    obj.countryNonPublic = personalResp.countryNonPublic;

    obj.city = personalResp.city;
    obj.cityNonPublic = personalResp.cityNonPublic;

    obj.company = personalResp.company;
    obj.companyNonPublic = personalResp.companyNonPublic;

    obj.positionHeld = personalResp.positionHeld;
    obj.positionHeldNonPublic = personalResp.positionHeldNonPublic;

    obj.updatedOn = obj.dateFromObj(personalResp, 'updatedOn');


    obj.saveToStorage(isOAuthClientUntrusted);
    return obj;
  }

  fromStorage(): NevisAccountPersonalBackendEntity {

    if (this.fillFromStorage()) {
      const parsed = this.parsed;

      this.nickName = parsed.nickName;
      this.nickNameNonPublic = parsed.nickNameNonPublic;

      this.firstName = parsed.firstName;
      this.firstNameNonPublic = parsed.firstNameNonPublic;

      this.middleName = parsed.middleName;
      this.middleNameNonPublic = parsed.middleNameNonPublic;

      this.lastName = parsed.lastName;
      this.lastNameNonPublic = parsed.lastNameNonPublic;

      this.gender = parsed.gender;
      this.genderNonPublic = parsed.genderNonPublic;

      this.dateOfBirth = this.dateFromObj(parsed, 'dateOfBirth');
      this.dateOfBirthNonPublic = parsed.dateOfBirthNonPublic;

      this.country = parsed.country;
      this.countryNonPublic = parsed.countryNonPublic;

      this.city = parsed.city;
      this.cityNonPublic = parsed.cityNonPublic;

      this.company = parsed.company;
      this.companyNonPublic = parsed.companyNonPublic;

      this.positionHeld = parsed.positionHeld;
      this.positionHeldNonPublic = parsed.positionHeldNonPublic;

      this.updatedOn = this.dateFromObj(parsed, 'updatedOn');

      return this;
    } else {
      return null;
    }
  }
}

