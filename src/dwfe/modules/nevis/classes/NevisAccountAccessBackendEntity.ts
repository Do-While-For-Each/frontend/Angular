import {DwfeAbstractStorageEntity} from '@dwfe/classes/DwfeAbstractStorageEntity';

import {BehaviorSubject, Observable} from 'rxjs';

import {DwfeNevisService} from '../services/nevis.service';
import {NevisAccountAccessModel} from '../models/NevisAccountAccessModel';
import {NevisClientType} from '../globals';

export class NevisAccountAccessBackendEntity extends DwfeAbstractStorageEntity implements NevisAccountAccessModel {

  access_token;
  token_type; // transient
  expires_in;
  refresh_token;
  scope; // transient
  data; // transient

  id;
  username;
  authorities;
  thirdParty;
  _avatarUrl;
  createdOn;

  oauth2ClientType: string;

  subjAvatarUrl = new BehaviorSubject<string>(this._avatarUrl); // transient

  nevisTransientFields = ['token_type', 'scope', 'data', 'subjAvatarUrl', 'nevisTransientFields']; // transient

  get storageKey(): string {
    return 'nevisAccountAccess';
  }

  get avatarUrl(): string {
    return this.subjAvatarUrl.getValue();
  }

  get avatarUrl$(): Observable<string> {
    return this.subjAvatarUrl.asObservable();
  }

  constructor() {
    super();
    this.transientFields.push.apply(this.transientFields, this.nevisTransientFields);
  }

  static of(data: NevisAccountAccessModel, nevisService: DwfeNevisService, oauth2ClientType: string): NevisAccountAccessBackendEntity {
    const obj = new NevisAccountAccessBackendEntity();
    const access = data.data;

    obj.access_token = data.access_token;
    if (data.expires_in) { // for unlimited access_token 'expires_in' is missing
      obj.expires_in = Date.now() + data.expires_in * 1000;
    }
    obj.refresh_token = data.refresh_token;

    obj.id = access.id;
    obj.username = access.username;
    obj.authorities = access.authorities;
    obj.thirdParty = access.thirdParty;
    obj.setAvatarUrl(access.avatarUrl, false);
    obj.createdOn = obj.dateFromObj(access, 'createdOn');

    obj.oauth2ClientType = oauth2ClientType;

    if (oauth2ClientType === NevisClientType.TRUSTED) {
      obj.scheduleTokenUpdate(nevisService, obj.get95PercentOfExpiryTime());
    } else if (oauth2ClientType === NevisClientType.UNTRUSTED) {
      obj.scheduleLogout(nevisService, obj.get95PercentOfExpiryTime());
    }

    obj.saveToStorage(oauth2ClientType === NevisClientType.UNTRUSTED);
    return obj;
  }

  fromStorage(nevisService: DwfeNevisService): NevisAccountAccessBackendEntity {
    if (this.fillFromStorage()) {
      const parsed = this.parsed;

      this.oauth2ClientType = parsed.oauth2ClientType;
      if (this.oauth2ClientType === NevisClientType.TRUSTED) {
        this.expires_in = parsed.expires_in;
        if (this.expires_in < Date.now()) {
          return null;
        }
      }
      this.access_token = parsed.access_token;
      this.refresh_token = parsed.refresh_token;

      this.id = parsed.id;
      this.username = parsed.username;
      this.authorities = parsed.authorities;
      this.thirdParty = parsed.thirdParty;
      this.setAvatarUrl(parsed._avatarUrl, false);
      this.createdOn = this.dateFromObj(parsed, 'createdOn');

      if (this.oauth2ClientType === NevisClientType.TRUSTED) {
        const time = this.get95PercentOfExpiryTime();
        const time1Day = (60 * 60 * 24) * 1000;
        if (time < time1Day) {
          this.scheduleTokenUpdate(nevisService, 10 * 1000);
        } else {
          this.scheduleTokenUpdate(nevisService, time);
        }
      }
      return this;
    } else {
      return null;
    }
  }

  setAvatarUrl(value: string, saveToStorage = true) {
    this._avatarUrl = value;
    this.subjAvatarUrl.next(value);
    if (saveToStorage) {
      this.saveToStorage();
    }
  }

  get95PercentOfExpiryTime(): number {
    return Math.round(this.getExpiryTime() * 0.95);
  }

  getExpiryTime(): number {
    return this.expires_in - Date.now();
  }

  scheduleTokenUpdate(nevisService: DwfeNevisService, time: number): void {
    if (time >= 0) {
      setTimeout(() => {
        nevisService.performTokenRefresh(this);
      }, time);
    }
  }

  scheduleLogout(nevisService: DwfeNevisService, time: number): void {
    if (time >= 0) {
      setTimeout(() => {
        nevisService.logout();
      }, time);
    }
  }
}
