const PREFIX = '/COMMON/';

export const DWFE_COMMON_ENDPOINTS = {
  googleCaptchaValidate: `${PREFIX}google-captcha-validate`,
};
