export const I18N_COMMON_GOOGLE_CAPTCHA_ERROR_CODES_MAP = {
  'empty-google-captcha': {
    EN: 'Empty google captcha response',
    RU: 'Пустой ответ google captch\'и',
  },
  'google-captcha-detected-robot': {
    EN: 'Google captcha detected a robot. Sorry',
    RU: 'Google captcha считает вас роботом. Извините',
  },
  'google-captcha-error-connection': {
    EN: 'Error connection - google captcha. Try again later',
    RU: 'Ошибка соединения - google captcha. Попробуйте снова позже',
  },
  'google-captcha-error-exchange': {
    EN: 'Error exchange - google captcha. Try again later',
    RU: 'Ошибка обмена - google captcha. Попробуйте снова позже',
  },
  'google-captcha-not-initialized': {
    EN: 'Not initialized - google captcha',
    RU: 'Не инициализировано - google captcha',
  },
  'missing-google-captcha': {
    EN: 'Missing google captcha response',
    RU: 'Отсутствует ответ google captch\'и',
  },
};
