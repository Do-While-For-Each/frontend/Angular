import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'common-google-captcha',
  templateUrl: './google-captcha.component.html',
  styleUrls: ['./google-captcha.component.scss']
})
export class DwfeCommonGoogleCaptchaComponent implements OnInit {

  @Input() captchaTxt = '';
  @Input() errorMessage: string;
  @Input() showOverlay = false;

  @Output() googleResponse = new EventEmitter<string>();

  ngOnInit() {
    this.googleResponse.emit(null);
  }
}
