import {Observable} from 'rxjs';

import {OPT_FOR_ANONYMOUSE_REQ} from '@dwfe/globals';
import {DwfeAbstractExchanger} from '@dwfe/classes/DwfeAbstractExchanger';

import {DWFE_COMMON_ENDPOINTS} from '../globals';

export class CommonGoogleCaptchaValidateExchanger extends DwfeAbstractExchanger {
  getHttpReq$(body?: any): Observable<Object> {
    return this.http.post(
      DWFE_COMMON_ENDPOINTS.googleCaptchaValidate,
      body,
      OPT_FOR_ANONYMOUSE_REQ);
  }
}
