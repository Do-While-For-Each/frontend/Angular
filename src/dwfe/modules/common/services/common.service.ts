import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {DwfeAbstractExchanger} from '@dwfe/classes/DwfeAbstractExchanger';

import {CommonGoogleCaptchaValidateExchanger} from '../classes/CommonExchangers';
import {I18N_COMMON_GOOGLE_CAPTCHA_ERROR_CODES_MAP} from '../components/google-captcha/i18n';

@Injectable({
  providedIn: 'root'
})
export class DwfeCommonService {

  constructor(public http: HttpClient) {
  }


  //
  // EXCHANGERS
  //

  get googleCaptchaValidateExchanger(): CommonGoogleCaptchaValidateExchanger {
    return new CommonGoogleCaptchaValidateExchanger(
      this.http,
      DwfeAbstractExchanger.getExchangeOptV1(I18N_COMMON_GOOGLE_CAPTCHA_ERROR_CODES_MAP)
    );
  }
}
