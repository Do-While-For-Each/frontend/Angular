import {NgModule} from '@angular/core';

import {MatButtonModule} from '@angular/material';

import {DwfeActionCloseComponent} from './action-close.component';

@NgModule({
  declarations: [
    DwfeActionCloseComponent,
  ],
  imports: [
    MatButtonModule,
  ],
  exports: [
    DwfeActionCloseComponent,
  ]
})
export class DwfeActionCloseModule {
}
