import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MatButtonModule} from '@angular/material';

import {DwfeAlertModule} from '../../alert/alert.module';

import {DwfeActionOkComponent} from './action-ok.component';

@NgModule({
  declarations: [
    DwfeActionOkComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    DwfeAlertModule,
  ],
  exports: [
    DwfeActionOkComponent,
  ],
})
export class DwfeActionOkModule {
}
