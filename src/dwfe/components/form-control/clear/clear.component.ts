import {Component, Input} from '@angular/core';
import {AbstractControl} from '@angular/forms';

@Component({
  selector: 'dwfe-form-control-clear',
  templateUrl: './clear.component.html',
  styleUrls: ['./clear.component.scss']
})
export class DwfeFormControlClearComponent {

  @Input() control: AbstractControl;
}
