import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DwfeFormControlClearComponent} from './clear.component';

@NgModule({
  declarations: [
    DwfeFormControlClearComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    DwfeFormControlClearComponent,
  ],
})
export class DwfeFormControlClearModule {
}
