export const T7E_DWFE_FORM_CONTROL_INPUT_EMAIL = {
  required: {
    EN: 'Required',
    RU: 'Обязательно',
  },
  email_not_valid: {
    EN: 'Please enter a valid e-mail',
    RU: 'Некорректная эл. почта',
  },
  length_err: {
    EN: 'Length must be',
    RU: 'Длина должна быть',
  },
};
