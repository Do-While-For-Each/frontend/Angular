import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {MatFormFieldModule, MatInputModule} from '@angular/material';

import {DwfeFormControlClearModule} from '../clear/clear.module';

import {DwfeFormControlInputPasswordComponent} from './input-password.component';

@NgModule({
  declarations: [
    DwfeFormControlInputPasswordComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    DwfeFormControlClearModule,
  ],
  exports: [
    DwfeFormControlInputPasswordComponent,
  ],
})
export class DwfeFormControlInputPasswordModule {
}
