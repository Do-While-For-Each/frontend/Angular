import {Component, Input, OnInit} from '@angular/core';

import {DwfeAbstractEditableControl} from '../../../classes/DwfeAbstractEditableControl';

@Component({
  selector: 'dwfe-form-control-toggle',
  templateUrl: './toggle.component.html',
})
export class DwfeFormControlToggleComponent extends DwfeAbstractEditableControl implements OnInit {

  @Input() position = 'above';
  @Input() color = 'primary';

  @Input() hintTrue = 'TRUE';
  @Input() hintFalse = 'FALSE';

  ngOnInit(): void {
    super.ngOnInit(); // here it is just in case
  }

  tooltipTxt(): string {
    return this.control.value ? this.hintTrue : this.hintFalse;
  }
}
