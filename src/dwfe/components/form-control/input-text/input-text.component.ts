import {Component, OnInit} from '@angular/core';

import {DwfeAbstractEditableControl} from '../../../classes/DwfeAbstractEditableControl';

@Component({
  selector: 'dwfe-form-control-input-text',
  templateUrl: './input-text.component.html',
})
export class DwfeFormControlInputTextComponent extends DwfeAbstractEditableControl implements OnInit {

  compareAs = 'textField';

  ngOnInit(): void {
    super.ngOnInit(); // here it is just in case
  }
}
