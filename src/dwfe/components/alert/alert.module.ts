import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DwfeAlertComponent} from './alert.component';

@NgModule({
  declarations: [
    DwfeAlertComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    DwfeAlertComponent,
  ],
})
export class DwfeAlertModule {
}
