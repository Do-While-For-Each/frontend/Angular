import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'dwfe-preview-pic-circled',
  templateUrl: './preview-pic-circled.component.html',
  styleUrls: ['./preview-pic-circled.component.scss']
})
export class DwfePreviewPicCircledComponent implements OnInit, OnDestroy {

  @Input() previewWHinPx: number;
  externalLinearSize: string;
  svgLinearSize: string;

  @Input() picUrl: string;
  @Input() picUrl$: Observable<string>;
  @Input() showClearBtn = true;

  @Output() takeClearClick = new EventEmitter<boolean>();

  subjLatchForUnsubscribe = new Subject();

  constructor() {
  }

  ngOnInit(): void {
    this.externalLinearSize = this.previewWHinPx + 16 + 'px';
    this.svgLinearSize = this.previewWHinPx + 'px';

    if (this.picUrl$) {
      this.picUrl$
        .pipe(takeUntil(this.subjLatchForUnsubscribe.asObservable()))
        .subscribe(picUrl => this.picUrl = picUrl);
    }
  }

  ngOnDestroy(): void {
    this.subjLatchForUnsubscribe.next();
  }

  isPicUrl() {
    return this.picUrl !== null && this.picUrl !== undefined
      && this.picUrl !== 'null' && this.picUrl !== 'undefined'
      && this.picUrl && this.picUrl !== 'false';
  }
}
