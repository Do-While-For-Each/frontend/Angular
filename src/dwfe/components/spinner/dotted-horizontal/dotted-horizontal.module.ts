import {NgModule} from '@angular/core';

import {DwfeSpinnerDottedHorizontalComponent} from './dotted-horizontal.component';

@NgModule({
  declarations: [
    DwfeSpinnerDottedHorizontalComponent,
  ],
  imports: [],
  exports: [
    DwfeSpinnerDottedHorizontalComponent,
  ],
})
export class DwfeSpinnerDottedHorizontalModule {
}
